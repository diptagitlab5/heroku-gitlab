const express = require('express');
const app = express();

const port = app.set('PORT', process.env.PORT || 3000);

app.get('/', (req, res) => {
    res.status(200).send({
        message: 'hey Dip!!  How you doing?',
    })
})

app.listen(app.get('PORT'), () => {
    console.log('Server running on port');
})